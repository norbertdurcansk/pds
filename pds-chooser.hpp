/**
 * Source code pds-chooser.hpp
 * Defines library functions and structures for choosing local nodes
 * Norbert Durcansky
 * xdurca01
 * Created on 4/05/2017.
 * VUT FIT 2017
 * 1MIT LS PDS
 *
 */
#ifndef PDS_CHOOSER_HPP
#define PDS_CHOOSER_HPP

#include <vector>
#include <map>
#include<iostream>
#include <algorithm>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ifaddrs.h>
#include <netdb.h>
#include <fstream>
#include <signal.h>

using namespace std;

/**
 * Program parameters structure
 */
typedef struct {
    std::string input = "";
    std::string output = "";
} tParameters;

/** Record for saving host info */
typedef struct {
    std::vector <string> ipv4;
    std::vector <string> ipv6;
    int16_t groupId = -1;
} tRecord;

void writeXML();

void printHelp();

int parseCmd(int argc, char **argv);

void error();

void isValidMac(string mac);

void isValidIp(string ip);

void addNode(string ipv4, string ipv6, string mac);

void parseXML();

void makeGroup(uint index1, uint index2);

void handler(string command);

void writeXML();

void chooser();

void exitHandler(int sig);

#endif //PDS_CHOOSER_HPP