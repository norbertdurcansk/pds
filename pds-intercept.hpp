/**
 * Source code pds-intercept.hpp
 * Defines library functions and structures for MiM 
 *
 * Norbert Durcansky
 * xdurca01
 * Created on 4/10/2017.
 * VUT FIT 2017
 * 1MIT LS PDS
 *
 */
#ifndef PDS_INTERCEPT_HPP
#define PDS_INTERCEPT_HPP

#include <vector>
#include <map>
#include<iostream>
#include <algorithm>
#include <cctype>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <net/if.h>
#include <linux/if_ether.h>
#include <linux/if_packet.h>
#include <net/ethernet.h>
#include <ifaddrs.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <fstream>
#include <signal.h>

#define MAC_SIZE 6

using namespace std;
/**
 * Program parameters structure
 */
typedef struct {
    std::string file = "";
    std::string interface = "";
} tParameters;

typedef struct {
    uint8_t srcIp[16];
    uint8_t desIp[16];

} tIpv6;

typedef struct {
    uint8_t srcIp[4];
    uint8_t desIp[4];
} tIpv4;


typedef struct {
    std::string mac;
    std::vector <string> ipv4;
    std::vector <string> ipv6;
} tRecord;

int parseCmd(int argc, char **argv);

void exitHandler(int sig);

void printHelp();

void error();

void isValidMac(string mac);

void isValidIp(string ip);

void addNode(string group, string ipv4, string ipv6, string mac);

void parseXML();

void loadMAC(string mac, uint8_t *newMac);

int createSocket();

bool compareAddr(uint8_t *addr1, string addr2, string protocol);

bool findPair(uint8_t *src, uint8_t *des, string protocol, uint8_t *mac);

int arpGetIndex();

void handler();

void getMyMAC(uint8_t *srcMac);

#endif //PDS_INTERCEPT_HPP