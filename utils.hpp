/**
 * Source code utils.hpp
 * Defines library functions and structures for scanning local network
 *
 * Norbert Durcansky
 * xdurca01
 * Created on 3/28/2017.
 * VUT FIT 2017
 * 1MIT LS PDS
 *
 */
#ifndef PDS_UTILS_HPP
#define PDS_UTILS_HPP

#include "pds-scanner.hpp"

vector <string> generateTargets(char *subnet, char *address);

int createSocket();

void addRecord(map <string, tRecord> *records, char *mac, char *ip, uint8_t type);

uint16_t getCheckSum(uint16_t *pseudoH, int size);

void writeXML(map <string, tRecord> *records, string file);

#endif //PDS_UTILS_HPP