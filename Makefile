#Makefile for project PDS
.PHONY = all

CC= g++
CFLAGS = -I/usr/include/libxml2 -pthread -std=c++11 -Wall -g

all: pds-scanner pds-chooser  pds-spoof  pds-massspoof pds-intercept  exc

pds-scanner: pds-scanner.o  utils.o
	$(CC) $(CFLAGS) -o pds-scanner pds-scanner.o utils.o

pds-scanner.o: pds-scanner.hpp  pds-scanner.cpp 
	$(CC) $(CFLAGS) -c pds-scanner.cpp 


utils.o: utils.hpp  utils.cpp 
	$(CC) $(CFLAGS) -c utils.cpp

pds-chooser: pds-chooser.o
	$(CC) $(CFLAGS) -o pds-chooser pds-chooser.o  -lxml2

pds-chooser.o: pds-chooser.hpp  pds-chooser.cpp
	$(CC) $(CFLAGS) -c pds-chooser.cpp  -lxml2

pds-intercept: pds-intercept.o
	$(CC) $(CFLAGS) -o pds-intercept pds-intercept.o -lxml2

pds-intercept.o: pds-intercept.hpp  pds-intercept.cpp
	$(CC) $(CFLAGS) -c pds-intercept.cpp  -lxml2

pds-massspoof: pds-massspoof.o
	$(CC) $(CFLAGS) -o pds-massspoof pds-massspoof.o  -lxml2

pds-massspoof.o: pds-massspoof.hpp  pds-massspoof.cpp
	$(CC) $(CFLAGS) -c pds-massspoof.cpp  -lxml2

pds-spoof: pds-spoof.o
	$(CC) $(CFLAGS) -o pds-spoof pds-spoof.o

pds-spoof.o: pds-spoof.hpp  pds-spoof.cpp
	$(CC) $(CFLAGS) -c pds-spoof.cpp


exc:
	chmod +x pds-scanner
	chmod +x pds-chooser
	chmod +x pds-spoof
	chmod +x pds-massspoof
	chmod +x pds-intercept

clean:
	$(RM) pds-scanner *.o *~
	$(RM) pds-chooser *.o *~
	$(RM) pds-spoof *.o *~
	$(RM) pds-massspoof *.o *~
	$(RM) pds-intercept *.o *~
