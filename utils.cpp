/**
 * Source code utils.cpp
 * Implements library functions and structures for scanning local network
 *
 * Norbert Durcansky
 * xdurca01
 * Created on 3/28/2017.
 * VUT FIT 2017
 * 1MIT LS PDS
 *
 */
#include "utils.hpp"

/** Creates socket */
int createSocket() {
    int sd;
    if ((sd = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL))) < 0) {
        fprintf(stderr, "failed to create socket");
        exit(-1);
    }
    return sd;
}

/** Adds record to map structure */
void addRecord(map <string, tRecord> *records, char *mac, char *ip, uint8_t type) {

    /** not defined mac is not permitted */
    if (strcmp(mac, "0000.0000.0000") == 0) {
        return;
    }
    if (records->find(mac) == records->end()) {
        tRecord record;
        if (type == 1) {
            record.ipv4.push_back(ip);
        } else {
            record.ipv6.push_back(ip);
        }
        records->insert(pair<string, tRecord>(mac, record));

    } else if (type == 1 &&
               std::find((*records)[mac].ipv4.begin(), (*records)[mac].ipv4.end(), ip) == (*records)[mac].ipv4.end()) {
        (*records)[mac].ipv4.push_back(ip);
    } else if (type == 2 &&
               std::find((*records)[mac].ipv6.begin(), (*records)[mac].ipv6.end(), ip) == (*records)[mac].ipv6.end()) {
        (*records)[mac].ipv6.push_back(ip);
    }
}

/** Computes checksum for checksum header in ICMPv6 */
/* partially copied, license inclosed */
/*  Copyright (C) 2011-2015  P.D. Buchan (pdbuchan@yahoo.com)

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/
uint16_t getCheckSum(uint16_t *pseudoH, int size) {
    uint32_t checksum = 0;
    while (size > 1) {
        checksum += *(pseudoH);
        pseudoH++;
        size -= 2;
    }
    if (size > 0) {
        checksum += *((uint8_t *) pseudoH);
    }
    while (checksum >> 16 != 0) {
        checksum = (checksum >> 16) + (checksum & 0xffff);
    }
    /* Make inverse */
    return ~checksum;
}

/** Generate targets for ARP requests */
vector <string> generateTargets(char *subnet, char *address) {
    vector <string> targets;
    struct sockaddr_in tmpadd;
    uint8_t ipv4[4];
    uint8_t mask[4];

    // get IP address
    inet_pton(AF_INET, subnet, &(tmpadd.sin_addr));
    memcpy(mask, &(tmpadd.sin_addr), 4 * sizeof(uint8_t));
    inet_pton(AF_INET, address, &(tmpadd.sin_addr));
    memcpy(ipv4, &(tmpadd.sin_addr), 4 * sizeof(uint8_t));

    uint8_t netid[4];
    for (int i = 0; i < 4; i++) {
        netid[i] = ipv4[i] & mask[i];
    }

    uint8_t last[4];
    for (int i = 0; i < 4; i++) {
        uint8_t inv = mask[i] ^0xFF;
        last[i] = netid[i] | inv;
    }
    while (true) {
        netid[3]++;
        if (netid[3] == 0) {
            netid[2]++;
            if (netid[2] == 0) {
                netid[1]++;
                if (netid[1] == 0) {
                    netid[0]++;
                }
            }
        }
        /* last is broadcast skip */
        if (netid[3] == last[3] && netid[2] == last[2] && netid[1] == last[1] && netid[0] == last[0]) {
            break;
        }
        /* add to list of possible targets */
        if (netid[0] != ipv4[0] || netid[1] != ipv4[1] || netid[2] != ipv4[2] || netid[3] != ipv4[3]) {
            string out = to_string(netid[0]) + "." + to_string(netid[1]) + "." + to_string(netid[2]) + "." +
                         to_string(netid[3]);
            targets.push_back(out);
        }
    }
    return targets;
}

/** Write out XML file */
void writeXML(map <string, tRecord> *records, string file) {

    ofstream output;
    output.open(file);
    output << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<devices>\n";

    map<string, tRecord>::const_iterator it;
    for (it = records->begin(); it != records->end(); it++) {
        output << "\t<host mac=\"" + it->first + "\">\n";

        vector <string> vector4 = it->second.ipv4;
        vector <string> vector6 = it->second.ipv6;

        for (std::vector<string>::iterator itt = vector4.begin(); itt != vector4.end(); ++itt) {
            output << "\t\t<ipv4>" + *itt + "</ipv4>\n";
        }
        for (std::vector<string>::iterator itt = vector6.begin(); itt != vector6.end(); ++itt) {
            output << "\t\t<ipv6>" + *itt + "</ipv6>\n";
        }
        output << "\t</host>\n";
    }
    output << "</devices>\n";
    output.close();
}