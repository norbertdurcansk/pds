/**
 * Source code pds-massspoof.cpp
 * Program for mass spoofing
 * Norbert Durcansky
 * xdurca01
 * Created on 3/29/2017.
 * VUT FIT 2017
 * 1MIT LS PDS
 *
 */
#include "pds-massspoof.hpp"
#include <libxml/parser.h>
#include <libxml/tree.h>

tParameters params;
map <string, vector<tRecord>> nodes;

/** Prints help */
void printHelp() {
    fprintf(stdout, "\
	-f <file>   	 name of the input file <file>\n\
	-p <protocol>  	 used protocol\n\
	-i <interface> 	 interface name\n\
	-t <msec>        time interval in msec\n\
	-h           	 this help message\n\
");
}

/** Parses user input parameters */
int parseCmd(int argc, char **argv) {
    int c;
    opterr = 0;
    while ((c = getopt(argc, argv, "hf:i:t:p:")) != -1)
        switch (c) {
            case 'f':
                params.file = optarg;
                break;
            case 'i':
                params.interface = optarg;
                break;
            case 't':
                params.msec = stoi(optarg);
                break;
            case 'p':
                params.protocol = optarg;
                break;
            case 'h':
                printHelp();
                exit(0);
            case '?':
                if (optopt == 'f' || optopt == 'i' || optopt == 't' || optopt == 'p')
                    fprintf(stderr, " -%c requires an argument.\n", optopt);
                else if (isprint(optopt))
                    fprintf(stderr, "Unknown option '-%c'.\n", optopt);
                else
                    fprintf(stderr,
                            "Unknown option character '\\x%x'.\n",
                            optopt);
                return -1;
            default:
                return -1;
        }
    return 0;
}

/** Error function, XML could not be parsed */
void error() {
    fprintf(stderr, "Failed to parse %s\n", params.file.c_str());
    exit(-1);
}

/** Validate MAC */
void isValidMac(string mac) {
    if (mac.length() == 17) {
        for (int i = 0; i < 17; i++) {
            if ((i % 3 != 2 && !isxdigit(mac[i])) || (i % 3 == 2 && mac[i] != ':'))
                error();
        }
    } else if (mac.length() == 14) {
        for (int i = 0; i < 14; i++) {
            if ((i % 5 != 4 && !isxdigit(mac[i])) || (i % 5 == 4 && mac[i] != '.')) {
                error();
            }
        }
    } else error();
}

/** Validate IP address */
void isValidIp(string ip) {
    struct addrinfo *result;
    int resp;
    if (ip == "")return;

    if ((resp = getaddrinfo(ip.c_str(), NULL, NULL, &result)) != 0) {
        error();
    }
    freeaddrinfo(result);
}

/** add parsed data to map structure of all records */
void addNode(string group, string ipv4, string ipv6, string mac) {
    /* validation */
    isValidMac(mac);
    isValidIp(ipv4);
    isValidIp(ipv6);

    if (nodes.find(group) == nodes.end()) {
        std::vector <tRecord> list;
        tRecord record;
        record.mac = mac;
        if (ipv4 != "") record.ipv4.push_back(ipv4);
        if (ipv6 != "") record.ipv6.push_back(ipv6);
        list.push_back(record);
        nodes[group] = list;
        /* node exists */
    } else {
        for (std::vector<tRecord>::iterator itt = nodes[group].begin(); itt != nodes[group].end(); ++itt) {

            /* found record, just add address */
            if ((*itt).mac == mac) {
                if (ipv4 != "") {
                    if (std::find((*itt).ipv4.begin(), (*itt).ipv4.end(), ipv4) == (*itt).ipv4.end()) {
                        (*itt).ipv4.push_back(ipv4);
                    }
                } else if (ipv6 != "") {
                    if (std::find((*itt).ipv6.begin(), (*itt).ipv6.end(), ipv6) == (*itt).ipv6.end()) {
                        (*itt).ipv6.push_back(ipv6);
                    }
                }
                return;
            }
        }
        /* if not pair in group */
        if (nodes[group].size() >= 2)error();

        /* new host in group */
        tRecord record;
        record.mac = mac;
        if (ipv4 != "") record.ipv4.push_back(ipv4);
        if (ipv6 != "") record.ipv6.push_back(ipv6);
        nodes[group].push_back(record);
    }
}

/** call for group ./pds-spoof */
void call(string group) {
	if(nodes[group].size()!=2)error();

	tRecord *record1 = &(nodes[group])[0];
	tRecord *record2 = &(nodes[group])[1];
	uint8_t index = 0;
	uint8_t callIndex1,callIndex2;

    if (params.protocol == "arp") {

     	if (record1->ipv4.size() == 0 || record2->ipv4.size() == 0)return;

     	while (record1->ipv4.size()> index || record2->ipv4.size()>index) {

			callIndex1 = index % record1->ipv4.size();
			callIndex2 = index % record2->ipv4.size();
	 		string command = "./pds-spoof -i " + params.interface + " -t " + to_string(params.msec) +
	                             " -p " + params.protocol + " -victim1ip " + record1->ipv4[callIndex1] + " -victim1mac " +
	                             record1->mac + " -victim2ip " + record2->ipv4[callIndex2] + " -victim2mac " + record2->mac + " &";

			system(command.c_str());
	 		index++;
     	}

    } else if (params.protocol == "ndp") {

		if (record1->ipv6.size() == 0 || record2->ipv6.size() == 0)return;

     	while (record1->ipv6.size()> index || record2->ipv6.size()>index) {

			callIndex1 = index % record1->ipv6.size();
			callIndex2 = index % record2->ipv6.size();
	 		string command = "./pds-spoof -i " + params.interface + " -t " + to_string(params.msec) +
	                             " -p " + params.protocol + " -victim1ip " + record1->ipv6[callIndex1] + " -victim1mac " +
	                             record1->mac + " -victim2ip " + record2->ipv6[callIndex2] + " -victim2mac " + record2->mac + " &";
			
			system(command.c_str());
	 		index++;
     	}
    } else {
        /* wrong protocol */
        error();
    }
}

/** Call spoof for each victim group */
void spoof() {
	map<string, vector<tRecord>>::iterator it;
	for (it = nodes.begin(); it != nodes.end(); it++) {
		call(it->first);
	}
}

/** Parses XML file to map structure */
void parseXML() {
    xmlDocPtr doc; /* tree */
    xmlNode *root, *host, *node, *ips;

    doc = xmlReadFile(params.file.c_str(), NULL, 0);
    if (doc == NULL) {
        error();
    }

    root = xmlDocGetRootElement(doc);
    if (strcmp("devices", (char *) root->name) != 0) {
        error();
    }

    host = root->children;

    for (node = host; node; node = node->next) {
        /* hosts */
        if (node->type == 1) {
            if (strcmp((char *) node->name, "host") == 0) {
                char *mac,*group;
                mac = (char *) xmlGetProp(node, (xmlChar *) "mac");
                group = (char *) xmlGetProp(node, (xmlChar *) "group");
                /* every host should have mac */
                if (mac == NULL)error();

                if(group!=NULL){
                	ips = node->xmlChildrenNode;
                	for (; ips; ips = ips->next) {

                   		if (ips->type == 1) {
                        	if (strcmp((char *) ips->name, "ipv4") == 0) {
                            	addNode(group,(const char *) xmlNodeGetContent(ips), "", mac);
                        	} else if (strcmp((char *) ips->name, "ipv6") == 0) {
                            	addNode(group,"", (const char *) xmlNodeGetContent(ips), mac);
                        	} else {
                            	error();
                        	}
                    	}
                	}
                }
            } else {
                error();
            }
        }
    }

    xmlFreeDoc(doc);
}

/** CTRL+C handler */
void exitHandler(int sig) {
    exit(0);
}

/** Main function */
int main(int argc, char **argv) {
    if (int ret = parseCmd(argc, argv) != 0) {
        return ret;
    } else if (params.file == ("") || params.interface == ("") || params.protocol == ("")) {
        fprintf(stderr, "Missing argument\n");
        exit(-1);
    }
    signal(SIGINT, exitHandler);
    parseXML();
    spoof();
    return 0;
}
