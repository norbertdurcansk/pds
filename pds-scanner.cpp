/**
 * Source code pds-scanner.cpp
 * Program for scanning local network
 * Norbert Durcansky
 * xdurca01
 * Created on 3/28/2017.
 * VUT FIT 2017
 * 1MIT LS PDS
 *
 */
#include "pds-scanner.hpp"
#include "utils.hpp"

map <string, tRecord> records;
tParameters params;
vector <string> ipv6Targets;

/* counter for reciever, when to start timeout */
atomic_uint sent;

/** Prints help */
void printHelp() {
    fprintf(stdout, "\
    	-i <interface>   name of the interface <interface>\n\
	-f <file>   	 name of the output file <file>\n\
	-h           	 this help message\n\
");
}

/** Parses user input parameters */
int parseCmd(int argc, char **argv) {
    int c;
    opterr = 0;
    while ((c = getopt(argc, argv, "hi:f:")) != -1)
        switch (c) {
            case 'i':
                params.interface = optarg;
                break;
            case 'f':
                params.file = optarg;
                break;
            case 'h':
                printHelp();
                exit(0);
            case '?':
                if (optopt == 'i' || optopt == 'f')
                    fprintf(stderr, " -%c requires an argument.\n", optopt);
                else if (isprint(optopt))
                    fprintf(stderr, "Unknown option '-%c'.\n", optopt);
                else
                    fprintf(stderr,
                            "Unknown option character '\\x%x'.\n",
                            optopt);
                return -1;
            default:
                return -1;
        }
    return 0;
}

/** Get index for interface */
int arpGetIndex() {
    int index = if_nametoindex(params.interface.c_str());
    if (index == 0) {
        fprintf(stderr, "Error getting interface index\n");
        exit(-1);
    } else return index;
}

/** Get info for arp request */
vector <string> arpGetInfo(tArpHeader *arpHeader, uint8_t *srcMac) {
    int sd;
    struct ifreq ifr;
    struct ifaddrs *ift, *iftt;
    char subnet[20];
    char src[INET_ADDRSTRLEN];

    if ((sd = socket(AF_INET, SOCK_RAW, IPPROTO_RAW)) < 0) {
        fprintf(stderr, "failed to create socket");
        exit(-1);
    }
    strcpy(ifr.ifr_name, params.interface.c_str());
    if (ioctl(sd, SIOCGIFHWADDR, &ifr) < 0) {
        fprintf(stderr, "failed getting source MAC address");
        exit(-1);
    }

    /* copy mac address */
    memcpy(srcMac, ifr.ifr_hwaddr.sa_data, 6 * sizeof(uint8_t));

    /* create arpHeader */
    arpHeader->hLen = 6;
    arpHeader->pLen = 4;
    arpHeader->hType = htons(1);
    arpHeader->pType = htons(ETH_P_IP);
    arpHeader->op = htons(1); /* request */

    memcpy(arpHeader->senderMac, srcMac, 6 * sizeof(uint8_t));
    memset(arpHeader->targetMac, 0, 6 * sizeof(uint8_t));

    /* get IP */
    ioctl(sd, SIOCGIFADDR, &ifr);
    memcpy(&(arpHeader->senderIp), &((struct sockaddr_in *) &ifr.ifr_addr)->sin_addr, 4 * sizeof(uint8_t));
    memcpy(src, inet_ntoa(((struct sockaddr_in *) &ifr.ifr_addr)->sin_addr), INET_ADDRSTRLEN);

    close(sd);

    getifaddrs(&ift);
    for (iftt = ift; iftt; iftt = iftt->ifa_next) {
        if (iftt->ifa_addr->sa_family == AF_INET && iftt->ifa_name == params.interface) {
            strcpy(subnet, inet_ntoa(((struct sockaddr_in *) iftt->ifa_netmask)->sin_addr));
        }
    }
    freeifaddrs(ift);

    /* generate all available targets in my network */
    return generateTargets(subnet, src);
}

/** Arp client for sending arp requests */
void arpClient() {
    tArpHeader arpHeader;
    int sd, resp;
    uint PACKET_LENGTH = 2 * MAC_SIZE + 2 + ARP_SIZE; /* fixed size */

    char target[INET_ADDRSTRLEN];
    uint8_t srcMac[MAC_SIZE], dstMac[MAC_SIZE], ethFrame[IP_MAXPACKET];

    struct sockaddr_ll device;
    struct addrinfo *result;

    /* get my targets */
    vector <string> targets = arpGetInfo(&arpHeader, srcMac);

    device.sll_ifindex = arpGetIndex();
    device.sll_family = AF_PACKET;
    memcpy(device.sll_addr, srcMac, 6 * sizeof(uint8_t));
    device.sll_halen = 6;

    memset(dstMac, 0xff, 6 * sizeof(uint8_t));

    /* set DEST and SRC MAC addrs */
    memcpy(ethFrame, dstMac, 6 * sizeof(uint8_t));
    memcpy(ethFrame + MAC_SIZE, srcMac, 6 * sizeof(uint8_t));

    /* set type 0x0806 ARP */
    ethFrame[12] = 0x08;
    ethFrame[13] = 0x06;

    /* iterate over targets */
    for (uint i = 0; i < targets.size(); i++) {

        strcpy(target, targets[i].c_str());
        if ((resp = getaddrinfo(target, NULL, NULL, &result)) != 0) {
            fprintf(stderr, "failed getaddrinfo \n");
            exit(-1);
        }
        memcpy(&arpHeader.targetIp, &((struct sockaddr_in *) result->ai_addr)->sin_addr, 4 * sizeof(uint8_t));
        freeaddrinfo(result);

        /* add ARP data */
        memcpy(ethFrame + 2 * MAC_SIZE + 2, &arpHeader, ARP_SIZE * sizeof(uint8_t));

        /* create socket RAW */
        sd = createSocket();
        // Send frame
        if ((resp = sendto(sd, ethFrame, PACKET_LENGTH, 0, (struct sockaddr *) &device, sizeof(device))) <= 0) {
            fprintf(stderr, "failed to send arp");
            exit(-1);
        }
        close(sd);
    }
    /* all sent, start timeout */
    sent++;
}

/** Reciever for ARP and NDP replies */
void reciever() {
    int sd;
    int status;
    uint8_t ethFrame[IP_MAXPACKET];
    uint8_t * tipv6;

    tArpHeader *arpHeader;
    tIcmpv6 *icmpv6;

    time_t start = time(NULL), end;
    double diff_t;

    sd = createSocket();

    struct timeval tv;
    tv.tv_sec = TIMEOUT+2; //TIMEOUT+2 (5) sec nothing, close socket
    tv.tv_usec = 0;

    /* set timeout and bind to interface */
    setsockopt(sd, SOL_SOCKET, SO_RCVTIMEO, (const char *) &tv, sizeof(struct timeval));
    setsockopt(sd, SOL_SOCKET, SO_BINDTODEVICE, params.interface.c_str(), params.interface.length());

    /* We can recieve icmpv6 message or arp reply */
    arpHeader = (tArpHeader *) (ethFrame + 2 * MAC_SIZE + 2);
    icmpv6 = (tIcmpv6 *) (ethFrame + 2 * MAC_SIZE + 2 + 40);
    tipv6 = (uint8_t * )(ethFrame + ETH_LENGTH + 4 + 4);

    while (true) {
        if ((status = recv(sd, ethFrame, IP_MAXPACKET, 0)) < 0) {
            break; /* timeout or smth happened */
        } else {
        	/* checks timeout after everything sent */
            if (sent<2) {
                time(&start);
            }
            /* arp reply */
            if (ethFrame[12] == 0x08 && ethFrame[13] == 0x06 && ntohs(arpHeader->op) == 2) {
                char mac[40];
                char ipv4[INET_ADDRSTRLEN];
                sprintf(mac, "%02x%02x.%02x%02x.%02x%02x", arpHeader->senderMac[0], arpHeader->senderMac[1],
                        arpHeader->senderMac[2], arpHeader->senderMac[3], arpHeader->senderMac[4],
                        arpHeader->senderMac[5]);
                inet_ntop(AF_INET, arpHeader->senderIp, ipv4, INET_ADDRSTRLEN);
                /* add record to structure */
                addRecord(&records, mac, ipv4, 1);
                memset(arpHeader, 0, sizeof(tArpHeader));

                /* icmpv6 - neighbor advertisements */
            } else if (ethFrame[12] == 0x86 && ethFrame[13] == 0xdd && icmpv6->type == 0x88) {
                char mac[40];
                char ipv6[INET6_ADDRSTRLEN];
                /* get mac */
                sprintf(mac, "%02x%02x.%02x%02x.%02x%02x", icmpv6->mac[0], icmpv6->mac[1], icmpv6->mac[2],
                        icmpv6->mac[3], icmpv6->mac[4], icmpv6->mac[5]);
                inet_ntop(AF_INET6, icmpv6->ipv6, ipv6, INET6_ADDRSTRLEN);
                addRecord(&records, mac, ipv6, 2);
                memset(icmpv6, 0, sizeof(tIcmpv6));

            /* echo or parameter problem message */
            }else if(ethFrame[12] == 0x86 && ethFrame[13] == 0xdd && (icmpv6->type==0x04 || icmpv6->type==0x81)){
            	char ipv6[INET6_ADDRSTRLEN];
            	char mac[40];
           		inet_ntop(AF_INET6, tipv6, ipv6, INET6_ADDRSTRLEN);
            	sprintf(mac, "%02x%02x.%02x%02x.%02x%02x", ethFrame[0], ethFrame[1],
            	ethFrame[2], ethFrame[3], ethFrame[4],
            	ethFrame[5]);
            	addRecord(&records, mac, ipv6, 2);
                memset(icmpv6, 0, sizeof(tIcmpv6));
            }else {
                time(&end);
                diff_t = difftime(end, start);
                if (diff_t > TIMEOUT) break;
                /* null structures */
                memset(icmpv6, 0, sizeof(tIcmpv6));
                memset(arpHeader, 0, sizeof(tArpHeader));
            }
        }
    }
    close(sd);
    /* sending can start again */
    sent = 0;
}

/** get my MAC address */
void getMyMAC(uint8_t *srcMac) {
    int sd;
    struct ifreq ifr;

    if ((sd = socket(AF_INET, SOCK_RAW, IPPROTO_RAW)) < 0) {
        fprintf(stderr, "failed to create socket");
        exit(-1);
    }
    strcpy(ifr.ifr_name, params.interface.c_str());
    if (ioctl(sd, SIOCGIFHWADDR, &ifr) < 0) {
        fprintf(stderr, "failed getting source MAC address");
        exit(-1);
    }
    memcpy(srcMac, ifr.ifr_hwaddr.sa_data, 6 * sizeof(uint8_t));
}

/** Sends ECHO and Malformed packet to local network in order to find Ipv6 nodes */
void ndpClient() {
    uint8_t packet[IP_MAXPACKET]; // packet we want to send

    uint8_t ethernetFrame[ETH_LENGTH];
    uint8_t target[INET6_ADDRSTRLEN];
    uint8_t source[INET6_ADDRSTRLEN];
    uint8_t deviceMac[MAC_SIZE];
    uint8_t icmpv6[IP_MAXPACKET];
    uint8_t checkSumHeader[IP_MAXPACKET];
    int sd;
    struct addrinfo *result;
    int status;
    struct ifaddrs *ift, *iftt;
    struct sockaddr_ll device;

    getMyMAC(deviceMac);
    uint8_t destMac[] = {0x33, 0x33, 0x00, 0x00, 0x00, 0x01};
    memcpy(ethernetFrame, destMac, 6 * sizeof(uint8_t));

    memcpy(ethernetFrame + 6, deviceMac, 6 * sizeof(uint8_t));

    icmpv6[24] = 1;
    icmpv6[25] = 1;
    //ipv6
    ethernetFrame[12] = 0x86;
    ethernetFrame[13] = 0xdd;
    memcpy(packet, ethernetFrame, ETH_LENGTH * sizeof(uint8_t));

    //set version 6
    uint8_t version[4];
    memset(&version, 0, sizeof(version));
    version[0] = 0x6e;

    memcpy(packet + ETH_LENGTH, version, 4 * sizeof(uint8_t));
    /* payload length */
    packet[18] = 0x00;
    packet[19] = 0x10;
    /* icmpv6 next */
    packet[20] = 0x3a;
    //hop limit
    packet[21] = 0xff;

    getifaddrs(&ift);
    for (iftt = ift; iftt; iftt = iftt->ifa_next) {
        if (iftt->ifa_addr->sa_family == AF_INET6 && iftt->ifa_name == params.interface) {
            memcpy(source, &((struct sockaddr_in6 *) iftt->ifa_addr)->sin6_addr.s6_addr, 16 * sizeof(uint8_t));
            memcpy(packet + 22, &((struct sockaddr_in6 *) iftt->ifa_addr)->sin6_addr.s6_addr, 16 * sizeof(uint8_t));
        }
    }
    device.sll_ifindex = arpGetIndex();
    device.sll_family = AF_PACKET;
    memcpy(device.sll_addr, deviceMac, 6 * sizeof(uint8_t));
    device.sll_halen = 6;

    /** echo on multicast */
    if ((status = getaddrinfo("ff02::1", NULL, NULL, &result)) != 0) {
        fprintf(stderr, "getaddrinfo() failed: %s\n", gai_strerror(status));
        exit(-1);
    }
    memcpy(target, ((sockaddr_in6 *) result->ai_addr)->sin6_addr.s6_addr, 16 * sizeof(uint8_t));
    memcpy(ethernetFrame + 3, target + 13, 3 * sizeof(uint8_t));
    memcpy(packet + 38, target, 16 * sizeof(uint8_t));
    //ICMP part
    icmpv6[0] = 0x80;
    icmpv6[1] = 0x00;
    //init checksum and reserved
    for (int i = 2; i < 4; i++) {
        icmpv6[i] = 0x00;
    }
    icmpv6[4] = 0xde;
    icmpv6[5] = 0xad;
    icmpv6[6] = 0xbe;
    icmpv6[7] = 0xef;

    /* random data */
    uint8_t data[] = {0x77, 0x10, 0x77, 0x10, 0x77, 0x10, 0x77, 0x10};

    memcpy(icmpv6 + 8, data, 8 * sizeof(uint8_t));
    memcpy(checkSumHeader, source, 16 * sizeof(uint8_t));
    memcpy(checkSumHeader + 16, target, 16 * sizeof(uint8_t));
    checkSumHeader[32] = 0;
    checkSumHeader[33] = 0;
    checkSumHeader[34] = ICMPV6_ECHO_SIZE / 256;
    checkSumHeader[35] = ICMPV6_ECHO_SIZE % 256;
    for (int i = 36; i < 39; i++) {
        checkSumHeader[i] = 0x00;
    }
    checkSumHeader[39] = IPPROTO_ICMPV6;
    memcpy(checkSumHeader + IP_LENGTH, icmpv6, ICMPV6_ECHO_SIZE * sizeof(uint8_t));
    uint16_t sum = getCheckSum((uint16_t *) checkSumHeader, IP_LENGTH + ICMPV6_ECHO_SIZE);
    icmpv6[2] = sum & 0b11111111;
    icmpv6[3] = sum >> 8;

    memcpy(packet + ETH_LENGTH + IP_LENGTH, icmpv6, ICMPV6_ECHO_SIZE * sizeof(uint8_t));
    /* create socket */
    sd = createSocket();
    /* Send frame */
    if ((status = sendto(sd, packet, ETH_LENGTH + IP_LENGTH + ICMPV6_ECHO_SIZE, 0, (struct sockaddr *) &device,
                         sizeof(device))) <= 0) {
        fprintf(stderr, "failed to send arp");
        exit(-1);
    }

    /* send malformed packet */
    packet[20] = 0;
    memset(&version, 0, sizeof(version));
    version[0] = 0x60;
    memcpy(packet + ETH_LENGTH, version, 4 * sizeof(uint8_t));
    packet[19] = 0x20;
    icmpv6[0] = 0x3a;
    icmpv6[1] = 0x01;

    /* random data */ 
    uint8_t malformed[]={0x80,0x01,0xef,0x09,0xef,0x09,0xef,0x09
    	,0x00,0x00,0x00,0x00,0x00,0x00,0x80,0x00,0xb2,0xc0,0xde,0xad,0xbe,0xef,0x80,0x01,0xef,0x09,0xef,0x09,0xef,0x09
    };
    /** copy random data */
 	memcpy(icmpv6+2, malformed, 30* sizeof(uint8_t));
    memcpy(packet + ETH_LENGTH + IP_LENGTH, icmpv6, ICMPV6_SIZE * sizeof(uint8_t));

    if ((status = sendto(sd, packet, 86, 0, (struct sockaddr *) &device,
                         sizeof(device))) <= 0) {
        fprintf(stderr, "failed to send icmpv6");
        exit(-1);
    }
    close(sd);

    /* everything was sent , start timeout */
    sent++;
}

/** CTRL+C handler */
void exitHandler(int sig) {
    writeXML(&records, params.file);
    exit(0);
}

/** Main function */
int main(int argc, char **argv) {
    if (int ret = parseCmd(argc, argv) != 0) {
        return ret;
    } else if (params.file == ("") || params.interface == ("")) {
        fprintf(stderr, "Missing argument\n");
        exit(-1);
    }
    signal(SIGINT, exitHandler);
    sent = 0;

    /* get all IPv4,IPv6 addresses */
    thread recievers(reciever);
    thread arp(arpClient);
	thread ndp(ndpClient);
    arp.join();
    ndp.join();
    recievers.join();
    writeXML(&records, params.file);

    return 0;
}
