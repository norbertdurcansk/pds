/**
 * Source code pds-massspoof.hpp
 * Defines library functions and structures for mass spoofing
 *
 * Norbert Durcansky
 * xdurca01
 * Created on 3/29/2017.
 * VUT FIT 2017
 * 1MIT LS PDS
 *
 */
#ifndef PDS_MASSSPOOF_HPP
#define PDS_MASSSPOOF_HPP

#include <vector>
#include <map>
#include<iostream>
#include <algorithm>
#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ifaddrs.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <fstream>
#include <signal.h>


using namespace std;
/**
 * Program parameters structure
 */
typedef struct {
    std::string interface = "";
    uint msec = 0;
    std::string protocol = "";
    std::string file = "";
} tParameters;

typedef struct {
    string mac = "";
    std::vector <string> ipv4;
    std::vector <string> ipv6;
} tRecord;

void exitHandler(int sig);

void parseXML();

void spoof();

void call(string group);

void addNode(string group,string ipv4, string ipv6, string mac);

void isValidIp(string ip);

void isValidMac(string mac);

void error();

int parseCmd(int argc, char **argv);

void printHelp();

#endif //PDS_MASSSPOOF_HPP