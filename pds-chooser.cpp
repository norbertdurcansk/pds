/**
 * Source code pds-chooser.cpp
 * Program for choosing local nodes
 * Norbert Durcansky
 * xdurca01
 * Created on 4/05/2017.
 * VUT FIT 2017
 * 1MIT LS PDS
 *
 */
#include "pds-chooser.hpp"
#include <libxml/parser.h>
#include <libxml/tree.h>

tParameters params;
map <string, tRecord> nodes;
uint16_t freeGroup;

/**
 * Show help */
void printHelp() {
    fprintf(stdout, "\
	-f <file>   	 name of the input file <file>\n\
	-o <file>   	 name of the output file <file>\n\
	-h           	 this help message\n\
");
}

/** Parse input parameters */
int parseCmd(int argc, char **argv) {
    int c;
    opterr = 0;
    while ((c = getopt(argc, argv, "hf:o:")) != -1)
        switch (c) {
            case 'f':
                params.input = optarg;
                break;
            case 'o':
                params.output = optarg;
                break;
            case 'h':
                printHelp();
                exit(0);
            case '?':
                if (optopt == 'f' || optopt == 'o')
                    fprintf(stderr, " -%c requires an argument.\n", optopt);
                else if (isprint(optopt))
                    fprintf(stderr, "Unknown option '-%c'.\n", optopt);
                else
                    fprintf(stderr,
                            "Unknown option character '\\x%x'.\n",
                            optopt);
                return -1;
            default:
                return -1;
        }
    return 0;
}

/** Error fun */
void error() {
    fprintf(stderr, "Failed to parse %s\n", params.input.c_str());
    exit(-1);
}

/** Checks if MAC id valid */
void isValidMac(string mac) {
    if (mac.length() == 17) {
        for (int i = 0; i < 17; i++) {
            if ((i % 3 != 2 && !isxdigit(mac[i])) || (i % 3 == 2 && mac[i] != ':'))
                error();
        }
    } else if (mac.length() == 14) {
        for (int i = 0; i < 14; i++) {
            if ((i % 5 != 4 && !isxdigit(mac[i])) || (i % 5 == 4 && mac[i] != '.')) {
                error();
            }
        }
    } else error();
}

/** Checks if IP is valid */
void isValidIp(string ip) {
    struct addrinfo *result;
    int resp;
    if (ip == "")return;

    if ((resp = getaddrinfo(ip.c_str(), NULL, NULL, &result)) != 0) {
        error();
    }
    freeaddrinfo(result);
}

/** add record to map from XML file */
void addNode(string ipv4, string ipv6, string mac) {

    //validation
    isValidMac(mac);
    isValidIp(ipv4);
    isValidIp(ipv6);

    /* node does not exist */
    if (nodes.find(mac) == nodes.end()) {
        tRecord record;
        if (ipv4 != "") record.ipv4.push_back(ipv4);
        if (ipv6 != "") record.ipv6.push_back(ipv6);
        nodes[mac] = record;
        /* node exists */
    } else {
        /* found record, just add address */
        if (ipv4 != "") {
            if (std::find(nodes[mac].ipv4.begin(), nodes[mac].ipv4.end(), ipv4) == nodes[mac].ipv4.end()) {
                nodes[mac].ipv4.push_back(ipv4);
            }
        } else if (ipv6 != "") {
            if (std::find(nodes[mac].ipv6.begin(), nodes[mac].ipv6.end(), ipv6) == nodes[mac].ipv6.end()) {
                nodes[mac].ipv6.push_back(ipv6);
            }
        }
        return;
    }
}

/** Parse XML file to map structure */
void parseXML() {
    xmlDocPtr doc; /* tree */
    xmlNode *root, *host, *node, *ips;

    doc = xmlReadFile(params.input.c_str(), NULL, 0);
    if (doc == NULL) {
        error();
    }

    root = xmlDocGetRootElement(doc);
    if (strcmp("devices", (char *) root->name) != 0) {
        error();
    }

    host = root->children;

    for (node = host; node; node = node->next) {
        /* hosts */
        if (node->type == 1) {
            if (strcmp((char *) node->name, "host") == 0) {
                char *mac;
                mac = (char *) xmlGetProp(node, (xmlChar *) "mac");
                /* every host should have mac */
                if (mac == NULL)error();
                /* set next layer, get IPs */
                ips = node->xmlChildrenNode;
                for (; ips; ips = ips->next) {
                    if (ips->type == 1) {
                        if (strcmp((char *) ips->name, "ipv4") == 0) {
                            addNode((const char *) xmlNodeGetContent(ips), "", mac);
                        } else if (strcmp((char *) ips->name, "ipv6") == 0) {
                            addNode("", (const char *) xmlNodeGetContent(ips), mac);
                        } else {
                            error();
                        }
                    }
                }
            } else {
                error();
            }
        }
    }

    xmlFreeDoc(doc);
}

/** Make group if possible */
void makeGroup(uint index1, uint index2) {

    map<string, tRecord>::iterator it;
    uint index = 0;
    /* flags indicate that both have IPs for protocol */
    uint8_t flag1 = 0;    //01,10,11
    uint8_t flag2 = 0;    //01,10,11

    if (nodes.size() < index1 + 1 || nodes.size() < index2 + 1)return;

    /* already has group */
    for (it = nodes.begin(); it != nodes.end(); it++, index++) {
        if (index == index1 || index == index2) {
            if (it->second.groupId != -1)return;

            if (index == index1) {
                if (it->second.ipv4.size() > 0) flag1 |= 0b01;
                if (it->second.ipv6.size() > 0) flag1 |= 0b10;
            } else {
                if (it->second.ipv4.size() > 0) flag2 |= 0b01;
                if (it->second.ipv6.size() > 0) flag2 |= 0b10;
            }
        }
    }
    /* no match in IPs means no group :) */
    if ((flag1 & flag2) == 0) return;
    index = 0;
    /* create new group */
    for (it = nodes.begin(); it != nodes.end(); it++, index++) {
        if (index == index1 || index == index2) {
            it->second.groupId = freeGroup;
        }
    }
    freeGroup++;
}

/** handle commands from user */
void handler(string command) {

    if (command == "exit") {
        writeXML();
        exit(0);
        /* delete group if exists */
    } else if (command.find("delete:") == 0) {
        command.erase(0, 7);

        if (command.length() > 0) {
            bool edited = false;
            int groupid = stoi(command);
            map<string, tRecord>::iterator it;
            for (it = nodes.begin(); it != nodes.end(); it++) {
                if (it->second.groupId == groupid) {
                    edited = true;
                    it->second.groupId = -1;
                }
            }
            for (it = nodes.begin(); it != nodes.end(); it++) {
                if (it->second.groupId > groupid && edited) {
                    it->second.groupId--;
                }
            }
            if (edited) {
                freeGroup--;
            }
        }

    } else if (command.find("group:") == 0) {
        command.erase(0, 6);
        if (command.find(".") > 0 && command.length() > 2) {
            makeGroup(stoi(command.substr(0, command.find("."))), stoi(command.substr(command.find(".") + 1)));
        }
    }

}

/** write edited XML to output file */
void writeXML() {

    ofstream output;
    output.open(params.output);
    output << "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n<devices>\n";

    map<string, tRecord>::const_iterator it;
    for (it = nodes.begin(); it != nodes.end(); it++) {

        if (it->second.groupId == -1) {
            output << "\t<host mac=\"" + it->first + "\">\n";
        } else {
            output << "\t<host mac=\"" + it->first + "\" group=\"victim-pair-" + to_string(it->second.groupId) +
                      "\">\n";
        }
        /** write all IPs */
        vector <string> vector4 = it->second.ipv4;
        vector <string> vector6 = it->second.ipv6;

        for (std::vector<string>::iterator itt = vector4.begin(); itt != vector4.end(); ++itt) {
            output << "\t\t<ipv4>" + *itt + "</ipv4>\n";
        }
        for (std::vector<string>::iterator itt = vector6.begin(); itt != vector6.end(); ++itt) {
            output << "\t\t<ipv6>" + *itt + "</ipv6>\n";
        }
        output << "\t</host>\n";
    }
    output << "</devices>\n";
    output.close();

}

/** Main function displays actual state of app */
void chooser() {

    printf("\tProgram pds-chooser\n\t----------------\n\
	Basic commands for choosing your pair:\n\
	group:<index1>.<index2>\t\t -create group from <index1> and <index2> (index1,index2 are uint8_t)\n\
	exit\t\t\t\t -close program and write to output\n\
	delete:<i>\t\t\t -delete group with id <i>\n\n\
	Examples:\n\
	group:0.2\t -create group from index 0 and index 2 from table\n\
	delete:1\t -delete group with id 1\n\n");

    printf("\tCreated groups:\n");
    printf("\tGROUP ID\tMAC1\t\t\tMAC2\n");
    map<string, tRecord>::const_iterator it;


    for (int i = 1; i < freeGroup; i++) {
        cout << "\t" + to_string(i);
        for (it = nodes.begin(); it != nodes.end(); it++) {
            if (it->second.groupId == -1)continue;
            if (it->second.groupId == i) {
                cout << "\t\t" + it->first;
            }
        }
        printf("\n");
    }
    /* parsed group table */
    printf("\n\tHost table:\n");
    printf("\tINDEX\tMAC\t\t\t\tIPV4\t\t\t\tIPV6\n");

    int index = 0;
    for (it = nodes.begin(); it != nodes.end(); it++, index++) {
        if (it->second.groupId != -1)continue;
        cout << "\t" + to_string(index);
        cout << "\t" + it->first;

        vector <string> vector4 = it->second.ipv4;
        vector <string> vector6 = it->second.ipv6;

        if (vector4.size() > 0) {
            cout << "\t\t\tavailable";
        } else {
            cout << "\t\t\t-\t";
        }

        if (vector6.size() > 0) {
            cout << "\t\t\tavailable\n";
        } else {
            cout << "\t\t\t-\n";
        }
    }
    printf("\n");
    string command;
    cout << "<console>: ";
    cin >> command;
    handler(command);
    return chooser();
}

/** CTR+C handler */
void exitHandler(int sig) {
    writeXML();
    exit(0);
}

/** Main */
int main(int argc, char **argv) {
    if (int ret = parseCmd(argc, argv) != 0) {
        return ret;
    } else if (params.input == ("") || params.output == ("")) {
        fprintf(stderr, "Missing argument\n");
        exit(-1);
    }
    parseXML();
    signal(SIGINT, exitHandler);
    freeGroup = 1;
    chooser();

    return 0;
}
