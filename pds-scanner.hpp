/**
 * Source code pds-scanner.hpp
 * Defines library functions and structures for scanning local network
 *
 * Norbert Durcansky
 * xdurca01
 * Created on 3/28/2017.
 * VUT FIT 2017
 * 1MIT LS PDS
 *
 */
#ifndef PDS_SCANNER_HPP
#define PDS_SCANNER_HPP

#include <vector>
#include <map>
#include<iostream>
#include <algorithm>
#include <getopt.h>
#include <thread>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <net/if.h>
#include <linux/if_ether.h>
#include <linux/if_packet.h>
#include <net/ethernet.h>
#include <ifaddrs.h>
#include <errno.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <bits/ioctls.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <fstream>
#include <signal.h>
#include <netinet/icmp6.h>
#include <ctime>
#include <atomic>

//Constants
#define ETH_LENGTH 14
#define ICMPV6_SIZE 32
#define ICMPV6_ECHO_SIZE 16
#define IP_LENGTH 40
#define ARP_SIZE 28
#define MAC_SIZE 6
#define TIMEOUT 3

using namespace std;
/**
 * Program parameters structure
 */
typedef struct {
    std::string file = "";
    std::string interface = "";
} tParameters;

typedef struct {
    uint16_t hType;
    uint16_t pType;
    uint8_t hLen;
    uint8_t pLen;
    uint16_t op;
    uint8_t senderMac[6];
    uint8_t senderIp[4];
    uint8_t targetMac[6];
    uint8_t targetIp[4];
} tArpHeader;

typedef struct {
    uint8_t type;
    uint8_t code;
    uint16_t checksum;
    uint32_t reserved;
    uint8_t ipv6[16];
    uint8_t opType;
    uint8_t opLenght;
    uint8_t mac[6];
} tIcmpv6;

typedef struct {
    std::vector <std::string> ipv4;
    std::vector <std::string> ipv6;
} tRecord;

void printHelp();

int parseCmd(int argc, char **argv, tParameters *params);

int arpGetIndex();

vector <string> arpGetInfo(tArpHeader *arpHeader, uint8_t *srcMac);

void arpClient();

void reciever();

void ndpClient();

void exitHandler(int sig);

void getMyMAC(uint8_t *srcMac);

#endif //PDS_SCANNER_HPP