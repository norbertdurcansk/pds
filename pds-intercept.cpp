/**
 * Source code pds-intercept.cpp
 * Program for MitM 
 * Norbert Durcansky
 * xdurca01
 * Created on 4/10/2017.
 * VUT FIT 2017
 * 1MIT LS PDS
 *
 */
#include "pds-intercept.hpp"

#include <libxml/parser.h>
#include <libxml/tree.h>

tParameters params;
map <string, vector<tRecord>> nodes;

/** Prints help for app */
void printHelp() {
    fprintf(stdout, "\
    	-i <interface>   name of the interface <interface>\n\
	-f <file>   	 name of the input file <file>\n\
	-h           	 this help message\n\
");
}

/** Parses user input parameters */
int parseCmd(int argc, char **argv) {
    int c;
    opterr = 0;
    while ((c = getopt(argc, argv, "hi:f:")) != -1)
        switch (c) {
            case 'i':
                params.interface = optarg;
                break;
            case 'f':
                params.file = optarg;
                break;
            case 'h':
                printHelp();
                exit(0);
            case '?':
                if (optopt == 'i' || optopt == 'f')
                    fprintf(stderr, " -%c requires an argument.\n", optopt);
                else if (isprint(optopt))
                    fprintf(stderr, "Unknown option '-%c'.\n", optopt);
                else
                    fprintf(stderr,
                            "Unknown option character '\\x%x'.\n",
                            optopt);
                return -1;
            default:
                return -1;
        }
    return 0;
}

/** CTR+C handler */
void exitHandler(int sig) {
    exit(0);
}

/** Error fun, unable to parse XML */
void error() {
    fprintf(stderr, "Failed to parse %s\n", params.file.c_str());
    exit(-1);
}

/** Validate MAC address */
void isValidMac(string mac) {
    if (mac.length() == 17) {
        for (int i = 0; i < 17; i++) {
            if ((i % 3 != 2 && !isxdigit(mac[i])) || (i % 3 == 2 && mac[i] != ':'))
                error();
        }
    } else if (mac.length() == 14) {
        for (int i = 0; i < 14; i++) {
            if ((i % 5 != 4 && !isxdigit(mac[i])) || (i % 5 == 4 && mac[i] != '.')) {
                error();
            }
        }
    } else error();
}

/** Validate IP address */
void isValidIp(string ip) {
    struct addrinfo *result;
    int resp;
    if (ip == "")return;

    if ((resp = getaddrinfo(ip.c_str(), NULL, NULL, &result)) != 0) {
        error();
    }
    freeaddrinfo(result);
}

/** add parsed data to map structure of all records */
void addNode(string group, string ipv4, string ipv6, string mac) {
    /* validation */
    isValidMac(mac);
    isValidIp(ipv4);
    isValidIp(ipv6);

    if (nodes.find(group) == nodes.end()) {
        std::vector <tRecord> list;
        tRecord record;
        record.mac = mac;
        if (ipv4 != "") record.ipv4.push_back(ipv4);
        if (ipv6 != "") record.ipv6.push_back(ipv6);
        list.push_back(record);
        nodes[group] = list;
        /* node exists */
    } else {
        for (std::vector<tRecord>::iterator itt = nodes[group].begin(); itt != nodes[group].end(); ++itt) {

            /* found record, just add address */
            if ((*itt).mac == mac) {
                if (ipv4 != "") {
                    if (std::find((*itt).ipv4.begin(), (*itt).ipv4.end(), ipv4) == (*itt).ipv4.end()) {
                        (*itt).ipv4.push_back(ipv4);
                    }
                } else if (ipv6 != "") {
                    if (std::find((*itt).ipv6.begin(), (*itt).ipv6.end(), ipv6) == (*itt).ipv6.end()) {
                        (*itt).ipv6.push_back(ipv6);
                    }
                }
                return;
            }
        }
        /* if not pair in group */
        if (nodes[group].size() >= 2)error();

        /* new host in group */
        tRecord record;
        record.mac = mac;
        if (ipv4 != "") record.ipv4.push_back(ipv4);
        if (ipv6 != "") record.ipv6.push_back(ipv6);
        nodes[group].push_back(record);
    }
}

/** Parsing XML file to Map */
void parseXML() {
    xmlDocPtr doc; /* tree */
    xmlNode *root, *host, *node, *ips;

    doc = xmlReadFile(params.file.c_str(), NULL, 0);
    if (doc == NULL) {
        error();
    }

    root = xmlDocGetRootElement(doc);
    if (strcmp("devices", (char *) root->name) != 0) {
        error();
    }

    host = root->children;

    for (node = host; node; node = node->next) {
        /* hosts */
        if (node->type == 1) {
            if (strcmp((char *) node->name, "host") == 0) {
                char *group, *mac;
                group = (char *) xmlGetProp(node, (xmlChar *) "group");
                mac = (char *) xmlGetProp(node, (xmlChar *) "mac");

                /* every host should have mac */
                if (mac == NULL)error();

                /* we have host with group */
                if (group != NULL) {
                    /* get IPs */
                    ips = node->xmlChildrenNode;
                    for (; ips; ips = ips->next) {

                        if (ips->type == 1) {
                            if (strcmp((char *) ips->name, "ipv4") == 0) {
                                addNode(group, (const char *) xmlNodeGetContent(ips), "", mac);
                            } else if (strcmp((char *) ips->name, "ipv6") == 0) {
                                addNode(group, "", (const char *) xmlNodeGetContent(ips), mac);
                            } else {
                                error();
                            }
                        }
                    }
                }
            } else {
                error();
            }
        }
    }

    xmlFreeDoc(doc);
}

/** load MAC address to uint8_t array
 * 2 formats available
 */
void loadMAC(string mac, uint8_t *newMac) {
    if (mac.length() == 17) {
        char substr[2];
        strncpy(substr, mac.c_str(), 2);
        newMac[0] = stoi(substr, NULL, 16);
        strncpy(substr, mac.c_str() + 3, 2);
        newMac[1] = stoi(substr, NULL, 16);
        strncpy(substr, mac.c_str() + 6, 2);
        newMac[2] = stoi(substr, NULL, 16);
        strncpy(substr, mac.c_str() + 9, 2);
        newMac[3] = stoi(substr, NULL, 16);
        strncpy(substr, mac.c_str() + 12, 2);
        newMac[4] = stoi(substr, NULL, 16);
        strncpy(substr, mac.c_str() + 15, 2);
        newMac[5] = stoi(substr, NULL, 16);

    } else if (mac.length() == 14) {
        char substr[2];
        strncpy(substr, mac.c_str(), 2);
        newMac[0] = stoi(substr, NULL, 16);
        strncpy(substr, mac.c_str() + 2, 2);
        newMac[1] = stoi(substr, NULL, 16);
        strncpy(substr, mac.c_str() + 5, 2);
        newMac[2] = stoi(substr, NULL, 16);
        strncpy(substr, mac.c_str() + 7, 2);
        newMac[3] = stoi(substr, NULL, 16);
        strncpy(substr, mac.c_str() + 10, 2);
        newMac[4] = stoi(substr, NULL, 16);
        strncpy(substr, mac.c_str() + 12, 2);
        newMac[5] = stoi(substr, NULL, 16);

    }
}

/** default socket */
int createSocket() {
    int sd;
    if ((sd = socket(PF_PACKET, SOCK_RAW, htons(ETH_P_ALL))) < 0) {
        fprintf(stderr, "failed to create socket");
        exit(-1);
    }
    return sd;
}

/** compare to addresses */
bool compareAddr(uint8_t *addr1, string addr2, string protocol) {
    int resp;
    struct addrinfo *result;
    uint8_t ipv4[4];
    uint8_t ipv6[16];

    if ((resp = getaddrinfo(addr2.c_str(), NULL, NULL, &result)) != 0) {
        error();
    }

    if (protocol == "ipv4") {
        memcpy(ipv4, &((struct sockaddr_in *) result->ai_addr)->sin_addr, 4 * sizeof(uint8_t));
        for (int i = 0; i < 4; i++) {
            if (ipv4[i] != addr1[i])return false;
        }
    } else if (protocol == "ipv6") {
        memcpy(ipv6, &((struct sockaddr_in6 *) result->ai_addr)->sin6_addr, 16 * sizeof(uint8_t));
        for (int i = 0; i < 16; i++) {
            if (ipv6[i] != addr1[i])return false;
        }
    }
    freeaddrinfo(result);
    return true;
}

/** Find pair in group
 * must be in same group
 * */
bool findPair(uint8_t *src, uint8_t *des, string protocol, uint8_t *mac) {
    map < string, vector < tRecord >> ::const_iterator
    it;

    int match = 0;
    /* check every group */
    for (it = nodes.begin(); it != nodes.end(); it++) {
        /* check all records in group */
        for (std::vector<tRecord>::iterator itt = nodes[it->first].begin(); itt != nodes[it->first].end(); ++itt) {

            if (protocol == "ipv4") {
                for (std::vector<string>::iterator ittt = (*itt).ipv4.begin(); ittt != (*itt).ipv4.end(); ++ittt) {
                    if (compareAddr(src, (*ittt).c_str(), "ipv4")) {
                        match++;
                    }
                    if (compareAddr(des, (*ittt).c_str(), "ipv4")) {
                        loadMAC((*itt).mac, mac);
                        match++;
                    }
                }
            } else {
                for (std::vector<string>::iterator ittt = (*itt).ipv6.begin(); ittt != (*itt).ipv6.end(); ++ittt) {
                    if (compareAddr(src, (*ittt).c_str(), "ipv6")) {
                        match++;
                    }
                    if (compareAddr(des, (*ittt).c_str(), "ipv6")) {
                        loadMAC((*itt).mac, mac);
                        match++;
                    }
                }
            }
            /* both IPs matched in same group */
            if (match >= 2) {
                return true;
            }
        }
        match = 0;

    }
    return false;
}

/** get index for interface */
int arpGetIndex() {
    int index = if_nametoindex(params.interface.c_str());
    if (index == 0) {
        fprintf(stderr, "Error getting interface index\n");
        exit(-1);
    } else return index;
}

void getMyMAC(uint8_t *srcMac) {
    int sd;
    struct ifreq ifr;

    if ((sd = socket(AF_INET, SOCK_RAW, IPPROTO_RAW)) < 0) {
        fprintf(stderr, "failed to create socket");
        exit(-1);
    }
    strcpy(ifr.ifr_name, params.interface.c_str());
    if (ioctl(sd, SIOCGIFHWADDR, &ifr) < 0) {
        fprintf(stderr, "failed getting source MAC address");
        exit(-1);
    }
    memcpy(srcMac, ifr.ifr_hwaddr.sa_data, 6 * sizeof(uint8_t));
}


/** handler for MiM */
void handler() {
    int status;
    int sd = createSocket();
    uint8_t ethFrame[IP_MAXPACKET];
    memset(ethFrame, 0, IP_MAXPACKET * sizeof(uint8_t));
    uint8_t srcMac[MAC_SIZE];
    //get my MAC address
    getMyMAC(srcMac);

    tIpv4 *ipv4 = (tIpv4 *) (ethFrame + 26); /* ipv4 packet structure */
    tIpv6 *ipv6 = (tIpv6 *) (ethFrame + 22); /* ipv6 packet structure */

    bool send = false;
    uint8_t desMac[MAC_SIZE];
    struct sockaddr_ll device;
    /* bind socket to interface */
    setsockopt(sd, SOL_SOCKET, SO_BINDTODEVICE, params.interface.c_str(), params.interface.length());

    device.sll_ifindex = arpGetIndex();
    device.sll_family = AF_PACKET;
    device.sll_halen = 6;

    while (true) {
        if ((status = recv(sd, ethFrame, IP_MAXPACKET, 0)) < 0) {
            fprintf(stderr, "Failed recieve data from socket\n");
            exit(-1);
        }
        //ipv4
        if (ethFrame[12] == 0x08 && ethFrame[13] == 0x00) {
            if (findPair(ipv4->srcIp, ipv4->desIp, "ipv4", desMac)) {
                send = true;
            }
            //ipv6
        } else if (ethFrame[12] == 0x86 && ethFrame[13] == 0xdd) {
            if (findPair(ipv6->srcIp, ipv6->desIp, "ipv6", desMac)) {
                send = true;
            }
        }
        if (send) {
            /* copy new mac from map structure */
            memcpy(ethFrame, desMac, 6 * sizeof(uint8_t));
            memcpy(ethFrame+6, srcMac, 6 * sizeof(uint8_t));

            memcpy(device.sll_addr, srcMac, 6 * sizeof(uint8_t));

            if ((status = sendto(sd, ethFrame, status, 0, (struct sockaddr *) &device, sizeof(device))) <= 0) {
                fprintf(stderr, "failed to send edited packet");
                exit(-1);
            }
            send = false;
        }
    }
}

/** Main function */
int main(int argc, char **argv) {
    if (int ret = parseCmd(argc, argv) != 0) {
        return ret;
    } else if (params.file == ("") || params.interface == ("")) {
        fprintf(stderr, "Missing argument\n");
        exit(-1);
    }
    signal(SIGINT, exitHandler);
    parseXML();
    handler();

    return 0;
}
