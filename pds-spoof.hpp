/**
 * Source code pds-spoof.hpp
 * Defines library functions and structures for poisoning local nodes
 *
 * Norbert Durcansky
 * xdurca01
 * Created on 3/28/2017.
 * VUT FIT 2017
 * 1MIT LS PDS
 *
 */
#ifndef PDS_SPOOF_HPP
#define PDS_SPOOF_HPP

#include <vector>
#include <map>
#include<iostream>
#include <algorithm>
#include <thread>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <net/if.h>
#include <linux/if_ether.h>
#include <linux/if_packet.h>
#include <net/ethernet.h>
#include <ifaddrs.h>
#include <netdb.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <sys/ioctl.h>
#include <bits/ioctls.h>
#include <netinet/ip.h>
#include <arpa/inet.h>
#include <fstream>
#include <signal.h>
#include <netinet/icmp6.h>

#define ETH_LENGTH 14
#define ICMPV6_SIZE 32
#define ICMPV6_ECHO_SIZE 16
#define IP_LENGTH 40
#define ARP_SIZE 28
#define MAC_SIZE 6

using namespace std;
/**
 * Program parameters structure
 */
typedef struct {
    std::string interface = "";
    int msec = 0;
    std::string protocol = "";
    std::string ip1 = "";
    std::string ip2 = "";
    uint8_t mac1[MAC_SIZE];
    uint8_t mac2[MAC_SIZE];
} tParameters;

typedef struct {
    uint16_t hType;
    uint16_t pType;
    uint8_t hLen;
    uint8_t pLen;
    uint16_t op;
    uint8_t senderMac[MAC_SIZE];
    uint8_t senderIp[4];
    uint8_t targetMac[MAC_SIZE];
    uint8_t targetIp[4];
} tArpHeader;


void printHelp();

bool isValidMac(string mac);

void loadMAC(string mac, uint8_t *newMac);

int parseCmd(int argc, char **argv);

int createSocket();

int arpGetIndex();

void getMyMAC(uint8_t *srcMac);

void arpClient(string type, string who);

uint16_t getCheckSum(uint16_t *pseudoH, int size);

void ndpClient(string type, string who);

void exitHandler(int sig);


#endif //PDS_SPOOF_HPP